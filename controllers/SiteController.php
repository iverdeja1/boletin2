<?php

namespace app\controllers;


use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\data\ActiveDataProvider;
use app\models\Ciclista;
use app\models\Equipo;
use app\models\Puerto;
use app\models\Lleva;
use yii\data\SqlDataProvider;
use yii\db\Query;


class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
    
    public function actionConsulta1(){
        
          //mediante DAO
          //se crea un proveedor de datos con la consulta correspondiente
        $dataProvider = new \yii\data\SqlDataProvider([
            
            'sql'=>'SELECT count(*)as total_ciclista from ciclista',
        ]);
        

        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['total_ciclista'],
            "titulo"=>"Consulta 1 con Dao ",
            "enunciado"=>"Número de ciclistas que hay",
            "sql"=>"SELECT count(*) as total_ciclista from ciclista",
            
            
        ]);
    }
    
    public function actionConsulta1a(){
         //mediante Active Record 
        $dataProvider = new ActiveDataProvider([
            'query'=> Ciclista::find()->Select('COUNT("*") as total_ciclista'),

        ]);

                return $this->render("resultado",[
                    "resultados"=>$dataProvider,
                    "campos"=>['total_ciclista'],
                    "titulo"=>"Consulta 1a con Active Record",
                    "enunciado"=>"Número de ciclistas que hay",
                    "sql"=>"SELECT count(*) as total_ciclista from ciclista",

                ]);
    }
    
    public function actionConsulta2() {
        //mediante DAO
          //se crea un proveedor de datos con la consulta correspondiente
        $dataProvider = new \yii\data\SqlDataProvider([
            
            'sql'=>'SELECT COUNT(*)AS Ciclistas_Banesto FROM CICLISTA WHERE NOMEQUIPO="BANESTO"',
        ]);
        
        //se renderiza la vista donde vamos a mostrar los datos
        //le pasamos varios parámetros para usar en la vista incluyendo 
        //el proveedor de datos
        //un array con los campos que se van a visualizar en el GridView
        //algunas variables que vamos a querer usar en la vista
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['Ciclistas_Banesto'],
            "titulo"=>"Consulta 2 con Dao ",
            "enunciado"=>"Número de ciclistas que hay del equipo Banesto",
            "sql"=>"SELECT COUNT(*)AS Ciclistas_Banesto FROM CICLISTA WHERE NOMEQUIPO='BANESTO'",
              
        ]);
    }
    
//    public function actionConsulta2a(){
//  //mediante Active Record
//        
//        $dataProvider = new ActiveDataProvider([
//            'query'=> Ciclista::find()->select('COUNT("*") AS Ciclistas_Banesto')
//            ->WHERE('nomequipo="Banesto"'),
//            
//        ]);
//        
//                return $this->render("resultado",[
//                    "resultados"=>$dataProvider,
//                    "campos"=>['Ciclistas_Banesto'],
//                    "titulo"=>"Consulta 2a con Active Record",
//                    "enunciado"=>"Número de ciclistas que hay del equipo Banesto",
//                    "sql"=>"SELECT COUNT(*)AS Ciclistas_Banesto FROM CICLISTA WHERE NOMEQUIPO='BANESTO'",
//                    
//                ]);
//      
//    }
//    
//    public function actionConsulta3(){
//           //Mediante Dao
//        $dataProvider = new \yii\data\SqlDataProvider([
//            
//            'sql'=>'SELECT AVG(EDAD)AS Edad_Media FROM CICLISTA',
//        ]);
//        
//        //se renderiza la vista donde vamos a mostrar los datos
//        //le pasamos varios parámetros para usar en la vista incluyendo 
//        //el proveedor de datos
//        //un array con los campos que se van a visualizar en el GridView
//        //algunas variables que vamos a querer usar en la vista
//        
//        return $this->render("resultado",[
//            "resultados"=>$dataProvider,
//            "campos"=>['Edad_Media'],
//            "titulo"=>"Consulta 3 con Dao ",
//            "enunciado"=>"Edad media de los ciclistas",
//            "sql"=>"SELECT AVG(EDAD)AS EDAD_MEDIA FROM CICLISTA",
//              
//        ]);
//        
//    }
//    
//    public function actionConsulta3a(){
//         // Mediante Active Record
//        $dataProvider = new ActiveDataProvider([
//            'query' => Ciclista::find()->select('AVG(edad) AS Edad_Media'),
//             
//        ]);
//
//        return $this->render("resultado", [
//            "resultados" => $dataProvider,
//            "campos" => ['Edad_Media'],
//            "titulo" => "Consulta 3a con Active Record",
//            "enunciado" => "Edad media de los ciclistas",
//            "sql" => "SELECT AVG(EDAD)AS Edad_Media FROM CICLISTA",
//        ]);
//        
//    }
//   
//    public function actionConsulta4(){
//        //Mediante Dao
//        $dataProvider = new \yii\data\SqlDataProvider([
//            
//            'sql'=>'SELECT AVG(EDAD)AS equipo_banesto from ciclista where nomequipo="Banesto"',
//        ]);
//        
//        //se renderiza la vista donde vamos a mostrar los datos
//        //le pasamos varios parámetros para usar en la vista incluyendo 
//        //el proveedor de datos
//        //un array con los campos que se van a visualizar en el GridView
//        //algunas variables que vamos a querer usar en la vista
//        
//        return $this->render("resultado",[
//            "resultados"=>$dataProvider,
//            "campos"=>['equipo_banesto'],
//            "titulo"=>"Consulta 4 con Dao ",
//            "enunciado"=>"La edad media de los del equipo Banesto",
//            "sql"=>"SELECT AVG(EDAD)AS equipo_banesto from ciclista where nomequipo='Banesto'",
//              
//        ]);
//        
//     
//    }
//    
//    public function actionConsulta4a(){
//       // Mediante Active Record
//        $dataProvider = new ActiveDataProvider([
//            'query' => Ciclista::find()->select('AVG(edad) AS equipo_banesto')
//            ->where('nomequipo="Banesto"'),
//               
//        ]);
//
//        return $this->render("resultado", [
//            "resultados" => $dataProvider,
//            "campos" => ['equipo_banesto'],
//            "titulo" => "Consulta 4a con Active Record",
//            "enunciado" => "La edad media de los del equipo Banesto",
//            "sql" => "SELECT AVG(EDAD)AS equipo_banesto from ciclista where nomequipo='Banesto'",
//        ]); 
//    }
//    
//    public function actionConsulta5(){
//        //Mediante Dao
//        $dataProvider = new \yii\data\SqlDataProvider([
//            
//            'sql'=>'SELECT AVG(edad) AS Edad_Media FROM ciclista GROUP BY nomequipo',
//        ]);
//        
//        //se renderiza la vista donde vamos a mostrar los datos
//        //le pasamos varios parámetros para usar en la vista incluyendo 
//        //el proveedor de datos
//        //un array con los campos que se van a visualizar en el GridView
//        //algunas variables que vamos a querer usar en la vista
//        
//        return $this->render("resultado",[
//            "resultados"=>$dataProvider,
//            "campos"=>['Edad_Media'],
//            "titulo"=>"Consulta 5 con Dao ",
//            "enunciado"=>"La edad media de los ciclistas por cada equipo",
//            "sql"=>"SELECT AVG(edad) AS Edad_Media FROM ciclista GROUP BY nomequipo",
//              
//        ]);
//    }
//    
//    public function actionConsulta5a(){
//        // Mediante Active Record
//        $dataProvider = new ActiveDataProvider([
//        'query' => Ciclista::find()->select('AVG(edad) AS Edad_Media')
//                ->groupBy('nomequipo'),
//        ]);
//
//        return $this->render("resultado", [
//            "resultados" => $dataProvider,
//            "campos" => ['Edad_Media'],
//            "titulo" => "Consulta 5a con Active Record",
//            "enunciado" => "La edad media de los ciclistas por cada equipo",
//            "sql" => "SELECT AVG(edad) AS Edad_Media FROM ciclista GROUP BY nomequipo",
//        ]); 
//    }
//    
//    public function actionConsulta6(){
//        //Mediante Dao
//        $dataProvider = new \yii\data\SqlDataProvider([
//            
//            'sql'=>'SELECT COUNT(*) AS ciclistas_equipo FROM CICLISTA GROUP BY NOMEQUIPO',
//        ]);
//        
//        //se renderiza la vista donde vamos a mostrar los datos
//        //le pasamos varios parámetros para usar en la vista incluyendo 
//        //el proveedor de datos
//        //un array con los campos que se van a visualizar en el GridView
//        //algunas variables que vamos a querer usar en la vista
//        
//        return $this->render("resultado",[
//            "resultados"=>$dataProvider,
//            "campos"=>['ciclistas_equipo'],
//            "titulo"=>"Consulta 6 con Dao ",
//            "enunciado"=>"El número de ciclistas por equipo",
//            "sql"=>"SELECT COUNT(*)AS ciclistas_equipo FROM CICLISTA GROUP BY NOMEQUIPO",
//              
//        ]);
//    }
//    
//    public function actionConsulta6a(){
//       // Mediante Active Record
//        $dataProvider = new ActiveDataProvider([
//            'query' => Ciclista::find()->select('Count("*") AS ciclistas_equipo')
//                ->groupBy('nomequipo'),
//        ]);
//
//        return $this->render("resultado", [
//            "resultados" => $dataProvider,
//            "campos" => ['ciclistas_equipo'],
//            "titulo" => "Consulta 6a con Active Record",
//            "enunciado" => "El número de ciclistas por equipo",
//            "sql" => "SELECT COUNT(*) AS ciclistas_equipo FROM CICLISTA GROUP BY NOMEQUIPO;",
//        ]); 
//    }
//    
//    public function actionConsulta7(){
//        //Mediante Dao
//        $dataProvider = new \yii\data\SqlDataProvider([
//            
//            'sql'=>'SELECT COUNT(*) AS Total FROM puerto',
//        ]);
//        
//        return $this->render("resultado",[
//            "resultados"=>$dataProvider,
//            "campos"=>['Total'],
//            "titulo"=>"Consulta 7 con Dao ",
//            "enunciado"=>"El número total de puertos",
//            "sql"=>"SELECT COUNT(*) AS Total FROM puerto",
//              
//        ]);
//    }
//    
//    public function actionConsulta7a(){
//        // Mediante Active Record
//        $dataProvider = new ActiveDataProvider([
//            'query' => Puerto::find()->select('COUNT("*") AS Total'),
//            
//      
//        ]);
//
//        return $this->render("resultado", [
//            "resultados" => $dataProvider,
//            "campos" => ['Total'], 
//            "titulo" => "Consulta 7a con Active Record",
//            "enunciado" => "El número total de puertos",
//            "sql" => "SELECT COUNT(*) AS Total FROM puerto",
//        ]); 
//    }
//
//    
//    public function actionConsulta8(){
//        //Mediante Dao
//        $dataProvider = new \yii\data\SqlDataProvider([
//            
//            'sql'=>'SELECT COUNT(*) AS puertos_total FROM puerto WHERE altura>1500',
//        ]);
//        
//        return $this->render("resultado",[
//            "resultados"=>$dataProvider,
//            "campos"=>['puertos_total'],
//            "titulo"=>"Consulta 8 con Dao ",
//            "enunciado"=>"El número total de puertos mayores de 1500",
//            "sql"=>"SELECT COUNT(*) AS puertos_total FROM puerto WHERE altura>1500",
//              
//        ]);
//    }
//    
//   
//    public function actionConsulta8a(){
//            // Mediante Active Record
//            $dataProvider = new ActiveDataProvider([
//                'query' => Puerto::find()->select('COUNT("*") AS puertos_total')
//                    ->where('altura>1500'),
//            ]);
//
//            return $this->render("resultado", [
//                "resultados" => $dataProvider,
//                "campos" => ['puertos_total'],
//                "titulo" => "Consulta 8a con Active Record",
//                "enunciado" => "El número total de puertos mayores de 1500",
//                "sql" => "SELECT COUNT(*) AS puertos_total FROM puerto WHERE altura>1500",
//            ]); 
//
//
//    }
//    
//
//    //sin Subconsulta
//
//   
//    public function actionConsulta9(){
//         //Mediante Dao
//        $dataProvider = new \yii\data\SqlDataProvider([
//            
//            'sql'=>'select distinct nomequipo from ciclista group by nomequipo having count(*)>4',
//        ]);
//        
//        return $this->render("resultado",[
//            "resultados"=>$dataProvider,
//            "campos"=>['nomequipo'],
//            "titulo"=>"Consulta 9 con Dao ",
//            "enunciado"=>"Listar el nombre de los equipos que tengan más de 4 ciclistas",
//            "sql"=>"select distinct nomequipo from ciclista group by nomequipo having count(*)>4",
//              
//        ]);
//    }
//    
//    public function actionConsulta9a(){
//
//        // Mediante Active Record
//        $dataProvider = new ActiveDataProvider([
//            'query' => Ciclista::find()-> select ('nomequipo')->distinct()
//                ->groupBy('nomequipo')
//                ->having('COUNT("*")>4'),
//   
//        ]);
//
//        return $this->render("resultado", [
//            "resultados" => $dataProvider,
//            "campos" => ['nomequipo'],
//            "titulo" => "Consulta 9a con Active Record",
//            "enunciado" => "Listar el nombre de los equipos que tengan más de 4 ciclistas",
//            "sql" => "select distinct nomequipo from ciclista group by nomequipo having count(*)>4;",
//        ]); 
//    
//    }
//
// 
//
//    public function actionConsulta10(){
//         // Mediante DAO
//        $dataProvider = new \yii\data\SqlDataProvider([
//            'sql' => 'Select distinct nomequipo from ciclista where edad between 28 and 32 group by nomequipo having count(*)>4',
//        ]);
//
//        return $this->render("resultado", [
//            "resultados" => $dataProvider,
//            "campos" => ['nomequipo'],
//            "titulo" => "Consulta 10 con DAO",
//            "enunciado" => "Listar el nombre de los equipos que tengan más de 4 ciclistas cuya edad esté entre 28 y 32",
//            "sql" => " Select distinct nomequipo from ciclista where edad between 28 and 32 group by nomequipo having count(*)>4",
//        ]);
//    }
//    public function actionConsulta10a(){
//            // Mediante Active Record
//         $dataProvider = new ActiveDataProvider([
//             'query' => Ciclista::find() ->select('nomequipo')->distinct()
//                 ->where('edad BETWEEN 28 AND 32')
//                 ->groupBy('nomequipo')
//                 ->having('COUNT("*")>4'),
//                 
//             
//                 
//         ]);
//
//         return $this->render("resultado", [
//             "resultados" => $dataProvider,
//             "campos" => ['nomequipo'],
//             "titulo" => "Consulta 10a con Active Record",
//             "enunciado" => "Listar el nombre de los equipos que tengan más de 4 ciclistas cuya edad esté entre 28 y 32",
//             "sql" => " Select distinct nomequipo from ciclista where edad between 28 and 32 group by nomequipo having count(*)>4",
//         ]); 
//    }
//    
//    public function actionConsulta11(){
//            // Mediante DAO
//       $dataProvider = new \yii\data\SqlDataProvider([
//           'sql' => 'select count(*) AS etapas_ganadas_ciclista from etapa group by dorsal',
//       ]);
//
//       return $this->render("resultado", [
//           "resultados" => $dataProvider,
//           "campos" => ['etapas_ganadas_ciclista'],
//           "titulo" => "Consulta 11 con DAO",
//           "enunciado" => "Indícame el número de etapas que ha ganado cada uno de los ciclistas",
//           "sql" => "select count(*) AS etapas_ganadas_ciclista from etapa group by dorsal",
//       ]);
//    }
//    public function actionConsulta11a(){
//        // Mediante Active Record
//    $dataProvider = new ActiveDataProvider([
//        'query' => \app\models\Etapa::find()
//            ->select('count("*") AS etapas_ganadas_ciclista')
//            ->groupBy('dorsal'),
//         ]);
//
//    return $this->render("resultado", [
//        "resultados" => $dataProvider,
//        "campos" => ['etapas_ganadas_ciclista'],
//        "titulo" => "Consulta 11a con Active Record",
//        "enunciado" => "Indícame el número de etapas que ha ganado cada uno de los ciclistas",
//        "sql" => "select count(*) AS etapas_ganadas_ciclista from etapa group by dorsal",
//    ]);
//    }
//    
//    public function actionConsulta12(){
//            // Mediante DAO
//       $dataProvider = new \yii\data\SqlDataProvider([
//           'sql' => 'select distinct dorsal from etapa group by dorsal having count(*)>1',
//       ]);
//
//       return $this->render("resultado", [
//           "resultados" => $dataProvider,
//           "campos" => ['dorsal'],
//           "titulo" => "Consulta 12 con DAO",
//           "enunciado" => "Indícame el dorsal de los ciclistas que hayan ganado más de una etapa",
//           "sql" => " select distinct dorsal from etapa group by dorsal having count(*)>1;",
//       ]);
//    }
//    public function actionConsulta12a(){
//        // Mediante Active Record
//    $dataProvider = new ActiveDataProvider([
//        'query' => \app\models\Etapa::find()
//            ->select('dorsal')
//            ->distinct()
//            ->groupBy('dorsal')
//            ->having('count("*")>1'),
//         ]);
//
//    return $this->render("resultado", [
//        "resultados" => $dataProvider,
//        "campos" => ['dorsal'],
//        "titulo" => "Consulta 11a con Active Record",
//        "enunciado" => "Indícame el dorsal de los ciclistas que hayan ganado más de una etapa",
//        "sql" => " select distinct dorsal from etapa group by dorsal having count(*)>1;",
//    ]);
//    }
}
